snowscraper = 'snowscraper'
limit = 20
offset = 0

hd = {
  pshb: "snow height at mountain ski runs",
  psqb: "snow quality at mountain ski runs",
  psht: "snow height at ski runs in valley",
  sho:  "snow height in town",
  tabc: "temperature at the mountain in &deg;C",
  tatc: "temperature in valley in &deg;C",
  lns:  "day of last snowfall",
  ait:  "downhill into valley",
  bpkm: "snowy ski runs in km",
  alws: "avalanche warning status"
}

ScraperWiki.attach(snowscraper, "src")

sdata = ScraperWiki.sqliteexecute("select * from src.swdata limit ? offset ?", [limit, offset])
keys = sdata["keys"]
rows = sdata["data"]

print '<h2>The latest snow report for Kitzb&uuml;hel:  (' + keys.size.to_s + ' columns)</h2>'
print '<table border="1" style="border-collapse:collapse;">'

print "<tr>"
hd.each_value { |desc| puts "<th>#{desc}</th>" }

puts "</tr>"

rows.each do |row|
  puts "<tr>"
  row.each do |value|
    puts "<td>#{value.to_s}</td>"
  end
  puts "</tr>"
end

puts "</table>"
